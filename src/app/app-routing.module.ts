import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FolderPage } from './folder/folder.page';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductUploadFileComponent } from './product-upload-file/product-upload-file.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  {path:'',loadChildren:()=>import('./folder/folder.module').then(m=>m.FolderPageModule)},
  {path:"folder/Home", component:FolderPage},
  {path:"folder/Product", component:ProductComponent},
  {path:"folder/Product/Details/:productId", component:ProductDetailsComponent},
  {path:"folder/Product/Upload", component:ProductUploadFileComponent},

  


  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'product-modal',
    loadChildren: () => import('./product-modal/product-modal.module').then( m => m.ProductModalPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
