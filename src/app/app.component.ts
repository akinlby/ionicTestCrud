import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/folder/Home', icon: 'home' },
    { title: 'Product', url: '/folder/Product', icon: 'storefront' },
    { title: 'Upload Csv', url: '/folder/Product/Upload', icon: 'storefront' },
  ];
  constructor() {}
}
